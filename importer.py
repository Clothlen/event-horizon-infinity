#!/usr/bin/env python3.5
# -*- encoding: utf-8 -*-

'''
This file will automatically create a item_list.txt if given a directory of
components.
'''

import os
import pathlib
import json
import argparse

parser = argparse.ArgumentParser(description='Creates a file list for run.py.')

parser.add_argument(
	'--item_list_loc',
	type=str,
	dest='item_list_loc',
	action='store',
	default='build/item_list.txt',
	help='Location of the file list to be written to.'
)

parser.add_argument(
	'--from_database_components',
	type=str,
	dest='from_database_components',
	action='store',
	default='import/Database/Component',
	help='Location of the Components directory of a Database to be read from.'
)

parser.add_argument(
	'--from_database_modifications',
	type=str,
	dest='from_database_modifications',
	action='store',
	default='import/Database/Component/Modifications',
	help='Location of Modifications directory for Components of a Database to be\
	read from.'
)

parser.add_argument(
	'--filter_slot',
	type=int,
	dest='filter_slot',
	action='store',
	default=None,
	help='Set this to 0 to 5 to filter a particular slot.'
)

parser.add_argument(
	'--filter_mod',
	type=int,
	dest='filter_mod',
	action='store',
	help='Set this to the id of a mod to filter only that mod.'
)

args = parser.parse_args()


def main():

	cwd = pathlib.Path(os.getcwd())

	database_component_loc = pathlib.Path.joinpath(
		cwd,
		args.from_database_components
	)
	database_mod_loc = pathlib.Path.joinpath(
		cwd,
		args.from_database_modifications
	)
	print('Getting components from '+str(database_component_loc))
	print('Getting modifications from'+str(database_mod_loc))
	print('Writing to '+args.item_list_loc)

	component_files = [f for f in os.listdir(
		str(database_component_loc)
	) if os.path.isfile(
		os.path.join(str(database_component_loc), f)
	)]

	mod_files = [f for f in os.listdir(
		str(database_mod_loc)
	) if os.path.isfile(
		os.path.join(str(database_mod_loc), f)
	)]

	mod_list = {}

	# Create a map of mod ids -> mod names
	for file in mod_files:
		with pathlib.Path.open(
			pathlib.Path.joinpath(
				database_mod_loc,
				file
			),
			'r'
		) as f:
			file_json = json.loads(f.read())
			# The [:-5] removes the last 5 characters from the string. Since
			# all of these are .json files, only the .json part of the file
			# name should be removed.
			mod_list[file_json['Id']] = file[:-5]

	final_item_list = ''

	for file in component_files:
		with pathlib.Path.open(
			pathlib.Path.joinpath(
				database_component_loc,
				file
			),
			'r'
		) as f:
			file_json = json.loads(f.read())
			# Some modules, like the Afterburner, do not have an id. We'll
			# figure out what to do with them later.
			if 'Id' in file_json:
				'''if args.filter_slot:
					file_json_cell_type = file_json['CellType']
					if file_json_cell_type == '':
						file_json_cell_type = -1
					if args.filter_slot == int(file_json_cell_type):
						for mod in file_json['PossibleModifications']:
							final_item_list = str(
								final_item_list + '\n' + file_json['CellType'] + ' ' + \
								str(file_json['Id']) + ' ' + \
								''.join(file_json['Name'][1:].split()) + ' ' + \
								str(mod) + ' ' + str(mod_list[mod])
							)
				else:
					for mod in file_json['PossibleModifications']:
						final_item_list = str(
							final_item_list + '\n' + file_json['CellType'] + ' ' + \
							str(file_json['Id']) + ' ' + \
							''.join(file_json['Name'][1:].split()) + ' ' + \
							str(mod) + ' ' + str(mod_list[mod])
						)'''
			for mod in file_json['PossibleModifications']:
				final_item_list = str(
					final_item_list + line_builder(file_json, mod_list, mod)
				)

	with pathlib.Path.open(
		pathlib.Path(args.item_list_loc),
		'w'
	) as f:
		f.write(final_item_list.strip())


def line_builder(file_json, mod_list, mod):
	if 'Id' not in file_json:
		return ''
	cell_type = str(file_json['CellType'])
	id = str(file_json['Id'])
	name = ''.join(file_json['Name'][1:].split())
	mod_n = str(mod)
	mod_list_mod = str(mod_list[mod])

	if args.filter_slot:
		if cell_type == '':
			cell_type = -1
		if int(cell_type) != int(args.filter_slot):
			return ''

	if args.filter_mod:
		if int(mod_n) == int(args.filter_mod):
			pass
		else:
			return ''

	return str(
		'\n' + str(cell_type) + ' ' + id + ' ' + name + ' ' + mod_n + ' ' + \
		mod_list_mod
	)


if __name__ == '__main__':
	main()
