#!/usr/bin/env python3.5
# -*- encoding: utf-8 -*-

import os
import argparse
import pathlib
import time
import json
import shutil
import math
import copy

parser = argparse.ArgumentParser(description='Creates a Database within the \
build directory.')

parser.add_argument(
	'--database_loc',
	type=str,
	dest='database_loc',
	action='store',
	default='build',
	help='Location of the Database to be written to.'
)

parser.add_argument(
	'--item_list_loc',
	type=str,
	dest='item_list_loc',
	action='store',
	default='item_list.txt',
	help='Location of file of integers and modifiers corresponding to item ids.\
	Format looks like this: "item_id modifier_id". Separate by new lines. See\
	item_list_example.txt for an example.'
)

parser.add_argument(
	'--debuglvl',
	type=int,
	dest='debuglvl',
	action='store',
	default=0,
	help='Debug level. 0 is lowest and least verbose. As it becomes higher, it \
	becomes more verbose.'
)

parser.add_argument(
	'--ids_begin_at',
	type=int,
	dest='ids_begin_at',
	action='store',
	default=20000,
	help='What should the id of the first automatically-generated ship be? Every\
	ship created after the first will have an id of 1 + the id of the previously\
	generated ship.'
)

parser.add_argument(
	'--size_of_ships',
	type=int,
	dest='size_of_ships',
	action='store',
	default=2162,
	help='How wide should the ships be?'
)
'''
parser.add_argument(
	'--copy_to',
	type=str,
	dest='copy_to',
	action='store',
	default=None,
	help='Should it copy the results into ANOTHER directory (for example, to the\
	game directory)?'
)
'''

args = parser.parse_args()


def exiter(status=0, reason='unknown'):
	print('Exiting due to: '+str(reason))
	try:
		sys.exit(status)
	except Exception:
		os._exit(status)


class Logger:
	def __init__(self, **kwargs):
		self.timetime = False
		self.last_err = False
		self.debug = kwargs['debug']
		self.thread_str = str(kwargs['thread_str'])
		# ANSI escape sequence colors. Imcompatible with windows.
		# A comment has marked the approximate color. It is only accurate to
		# Ubuntu.
		# See https://en.wikipedia.org/wiki/ANSI_escape_code#Escape_sequences
		# and
		# https://stackoverflow.com/questions/287871/print-in-terminal-with-colors.
		self.colors = {
			'endc': '\033[0m',  # Resets it I suppose
			'bold': '\033[1m',
			'underline': '\033[4m',
			'error': '\033[91m',  # rgb(255,0,0)
			'warn': '\033[93m',  # rgb(255,255,0)
			'note': '\033[92m',  # rgb(0,255,0)
			'notice': '\033[32m',  # rgb(57,181,74)
			'dbg': '\033[95m',  # rgb(255,0,255)
			'test': '\033[94m'  # rgb(0,0,255)
		}
		# Windows does not support ANSI escape sequences.
		if os.name == 'nt':
			for color in self.colors:
				self.colors[color] = ''

		self.types_str = {
			'info': 'INFO',
			'warn': self.colors['warn']+'WARN'+self.colors['endc'],
			'err': self.colors['error']+'ERROR'+self.colors['endc'],
			'note': self.colors['note']+'NOTE'+self.colors['endc'],
			'notice': self.colors['notice']+'NOTICE'+self.colors['endc'],
			'dbg': self.colors['dbg']+'DEBUG ',  # Debug gets extra data
			'test': self.colors['test']+'TEST'+self.colors['endc']
		}

	def pr(self, text='???', type_str='INFO'):
		self.timetime = time.gmtime(time.time())
		print(
			self.colors['endc'] +\
			str(self.timetime.tm_hour).zfill(2) + ':' + \
			str(self.timetime.tm_min).zfill(2) + \
			':' + str(self.timetime.tm_sec).zfill(2),
			'[' + self.thread_str + '/' + type_str + ']:',
			str(text)
		)

	def info(self, text):
		self.pr(text=text, type_str=self.types_str['info'])

	def warn(self, text):
		self.pr(text=text, type_str=self.types_str['warn'])

	def err(self, text):
		self.last_err = text
		self.pr(text=text, type_str=self.types_str['err'])

	def note(self, text):
		self.pr(text=text, type_str=self.types_str['note'])

	def notice(self, text):
		self.pr(text=text, type_str=self.types_str['notice'])

	def dbg(self, text, debuglvl=args.debuglvl):
		if debuglvl < args.debuglvl:
			self.pr(
				text=text,
				type_str=self.types_str['dbg']+str(debuglvl)+self.colors['endc']
			)

	def test(self, text):
		self.pr(text=text, type_str=self.types_str['test'])

	def test_colors(self):
		for color in self.colors:
			self.test(
				self.colors['endc'] +\
				'Color "' +\
				str(color) +\
				'": "' +\
				self.colors[color] +\
				str(color) +\
				self.colors['endc'] +\
				'"'
			)


def main():

	start_time = time.time()

	# Get our current working directory
	cwd = pathlib.Path(os.getcwd())
	# Create our log.
	log = Logger(debug=args.debuglvl, thread_str='main')
	# log.test_colors()

	# This is an example ship.
	alpha_ship = """
{
	"ItemType": 6,
	"Id": -1,
	"IconImage": "space_invader",
	"IconScale": 0.5,
	"Name": "",
	"ModelImage": "space_invader",
	"ModelScale": 1,
	"EnginePosition": {
		"x": -1
	},
	"EngineColor": "#000000",
	"EngineSize": 0.1,
	"BuiltinDevices": [],
	"Layout": "",
	"Barrels": [
		{
			"Position": {
				"x": 0.2
			},
			"Offset": 0.8,
			"PlatformType": 2,
			"WeaponClass": ""
		}
	]
}
"""
	alpha_ship_build = """
{
	"ItemType": 8,
	"Id": 199912341,
	"ShipId": 19991234,
	"NotAvailableInGame": false,
	"DifficultyClass": 0,
	"BuildFaction": -1,
	"Components": [

	]
}
"""
	alpha_ship_tech = """
{
	"ItemType": 10,
	"Id": 199912342,
	"Type": 1,
	"ItemId": 19991234,
	"Faction": 0,
	"Dependencies": []
}

"""
	# Load it into json for easily changing it.
	alpha_ship_json = json.loads(alpha_ship)
	alpha_ship_build_json = json.loads(alpha_ship_build)
	alpha_ship_tech_json = json.loads(alpha_ship_tech)

	# Location of where our database will go.
	database_loc = pathlib.Path(args.database_loc)
	database_end_loc = pathlib.Path.joinpath(
		database_loc, 'Database/Infinity'
	).absolute()
	# Check if it exists and if it is a dir.
	if not database_loc.is_dir():
		log.err(
			'Database location at {} does not exist. Create the directory?'
			.format(database_loc)
		)
		# Ask user if they want to create a dir.
		create_database_loc = input('[Y/n] ')

		if create_database_loc.lower() == 'y':
			log.notice('Creating directory.')

			# Is it an absolute path? If so, just make the directory where they
			# told us to. If not, join the path with our cwd.
			if not database_loc.is_absolute():
				pathlib.Path.mkdir(pathlib.Path.joinpath(cwd, database_loc))
			else:
				log.note('Hope you know what you are doing. mkdir at '+str(database_loc))
				pathlib.Path.mkdir(database_loc)

		# We need somewhere to put our results.
		elif create_database_loc.lower() == 'n':
			log.err('Can not continue then, quitting.')
			exiter(0, 'Needs directory to place result in')

		# They must give an allowed answer.
		else:
			log.err('Can not understand "'+create_database_loc+'"')
			exiter(0, 'Needs answer')

	# Now, we go to database_loc and delete the Database/Infinity directory
	# within it.
	log.info(
		'Deleting "{}/Database/Infinity" (this is supposed to happen)'
		.format(database_loc)
	)
	try:
		shutil.rmtree(
			str(database_end_loc)
		)
	except FileNotFoundError:
		log.info('Could not find it, so can not delete it (this is okay).')
	# Then, we create the directory again.
	log.info('Rebuilding it.')
	os.makedirs(str(database_end_loc))

	# This is similar to what we just did above, but with the item_list.txt.
	item_list_loc = pathlib.Path(args.item_list_loc)
	# Check if it does not exist or is not a file.
	if not item_list_loc.is_file():
		log.err(
			'Item list location at {} does not exist. Quitting.'
			.format(item_list_loc)
		)
		exiter(0, 'Item list must exist')

	# This figures out the number of rows and columns for the ship.
	# ship_width = =CEILING.MATH(SQRT(A2))
	# math.ceil returns a float if it is given a float
	ship_width = int(math.ceil(math.sqrt(args.size_of_ships)))
	ship_height = ship_width

	# Item list variable.
	item_list = False
	# Open the item list (read only mode).
	with pathlib.Path.open(item_list_loc, 'r') as f:
		item_list = f.read().strip()  # Trim odd newlines + extra spaces.
	log.dbg(item_list, 1)

	ship_id_offset = 0
	ids_used = []

	rows_used = 0
	columns_used = 0

	# Split the item list by new lines.
	item_list_lines = item_list.split('\n')
	# And iterate over it.
	for item in item_list_lines:
		log.info('Iterating item "{}"'.format(str(item)))
		iteration_start = int(time.time())
		rows_used = 0
		columns_used = 0
		# Copy the original example ship
		new_ship_json = copy.deepcopy(alpha_ship_json)
		new_ship_build_json = copy.deepcopy(alpha_ship_build_json)
		new_ship_tech_json = copy.deepcopy(alpha_ship_tech_json)

		# Set the id.
		new_ship_json['Id'] = args.ids_begin_at + ship_id_offset
		new_ship_build_json['Id'] = new_ship_json['Id']
		new_ship_build_json['ShipId'] = new_ship_json['Id']
		new_ship_tech_json['Id'] = new_ship_json['Id']
		new_ship_tech_json['ItemId'] = new_ship_json['Id']
		ship_id_offset += 1

		'''
			Every line contains three major variables.
			* The slot type
			* The item id
			* The item modification
		'''
		item_line = item.split(' ')
		item_slot = item_line[0]
		item_id = item_line[1]
		item_basic_name = item_line[2]
		item_mod = item_line[3]
		item_mod_desc = item_line[4]

		new_ship_json['Name'] = str(
			str(item_basic_name) + str(item_mod_desc)
		)

		'''
		{
			"ComponentId": 119,
			"Modification": 0,
			"Quality": 5,
			"Locked": false,
			"X": 0,
			"Y": 0,
			"BarrelId": 0,
			"Behaviour": 0,
			"KeyBinding": 0
		}
		'''

		rows_used = 0
		columns_used = 0
		size_of_ships = args.size_of_ships
		for i in range(0, size_of_ships):
			size_of_ships = args.size_of_ships
			new_ship_json['Layout'] += item_slot
			new_ship_build_json['ShipId'] = new_ship_json['Id']

			new_ship_build_x = columns_used
			new_ship_build_y = rows_used
			columns_used += 1
			if columns_used == ship_width:
				columns_used = 0
				rows_used += 1
			if rows_used == ship_height:
				rows_used = 0

			# @TODO Needs improvement
			new_ship_build_json['Components'] += json.loads(
				"""
					[{{
						"ComponentId": {item_id},
						"Modification": {item_mod},
						"Quality": 5,
						"Locked": false,
						"X": {new_ship_build_x},
						"Y": {new_ship_build_y},
						"BarrelId": 0,
						"Behaviour": 0,
						"KeyBinding": 0
					}}]
			""".format(
					item_id=item_id,
					item_mod=item_mod,
					new_ship_build_x=new_ship_build_x,
					new_ship_build_y=new_ship_build_y
				)
			)
		# Finally, write the file.

		with pathlib.Path.open(
			pathlib.Path.joinpath(
				database_end_loc,
				new_ship_json['Name'] + '-ship.json'
			),
			'w'
		) as f:
			f.write(json.dumps(
				obj=new_ship_json,
				indent=4
			))

		with pathlib.Path.open(
			pathlib.Path.joinpath(
				database_end_loc,
				new_ship_json['Name'] + '-build.json'
			),
			'w'
		) as f:
			f.write(json.dumps(
				obj=new_ship_build_json,
				indent=4
			))

		with pathlib.Path.open(
			pathlib.Path.joinpath(
				database_end_loc,
				new_ship_json['Name'] + '-tech.json'
			),
			'w'
		) as f:
			f.write(json.dumps(
				obj=new_ship_tech_json,
				indent=4
			))
			iteration_end = iteration_start - int(time.time())
			if iteration_end > 0:
				log.info('  Took {} seconds'.format(iteration_end))

	end_time = time.time()
	log.info('In total, took about {} seconds to do this.'.format(
		int(end_time) - int(start_time)
	))

	# This does not work, and I am not sure I want it to. @TODO
	'''
	# If args.copy_to was set, we need to copy it now.
	if args.copy_to:
		copy_to = pathlib.Path.joinpath(
			pathlib.Path(args.copy_to),
			'Database/Infinity'
		)
		if copy_to.is_dir():
			shutil.copytree(str(database_end_loc.absolute()), str(copy_to.absolute()))
		else:
			log.warn(
				'Could not copy to directory {} because it is not a directory.'
				.format(str(copy_to))
			)
			log.warn('Create directory?')
			create_copy_to = input('[Y/n] ')
			if create_copy_to.lower() == 'y':
				log.note('Creating directory...')
				shutil.copytree(str(database_end_loc.absolute()), str(copy_to.absolute()))
			elif create_copy_to.lower() == 'n':
				log.err('Can not continue, shutting down.')
				exiter(0, 'Can not copy')
			else:
				log.err('Could not understand command "{}"'.format(create_copy_to))
				exiter(0, 'Can not understand')
	'''


if __name__ == '__main__':
	main()
else:
	print('Must be run directly, sorry. Or you can execute main() yourself.')
