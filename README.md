## Introduction

This project is intended to create an infinite amount of items in Event Horizon.

This is intended for use by the Discord.

## Prerequisites

* [pipenv](https://docs.pipenv.org/)

## Running

    1. pipenv install --python 3.5.2
    2. pipenv run python3.5 run.py

The script allows for arguments. The full list is available by typing in:
`pipenv run python3.5 run.py --help`
An example of how arguments work:
`pipenv run python3.5 run.py --ids_begin_at 9999`
